﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SrralleIndiaNote._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="card-body">
        <%--<asp:ScriptManager ID="ScriptTimeSheet" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>--%>
        <div class="form-group row">
            <label class="col-md-3 m-t-15">Note</label>
            <div class="table-responsive">
                <%--<asp:UpdatePanel ID="updTimeSheet" UpdateMode="Always" runat="server">
                    <ContentTemplate>--%>
                <asp:Panel ID="pnl_warning" runat="server" Visible="false">
                    <div class="alert alert-warning" role="alert" style="width: 100%">
                        <asp:Label ID="labWarning" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnl_err" runat="server" Visible="false">
                    <div class="alert alert-danger" role="alert" style="width: 100%">
                        <asp:Label ID="LabErr" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <asp:GridView ID="gvNote" runat="server" ShowFooter="true" CssClass="table-striped table-bordered table"
                    AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" ShowHeader="true" OnRowCreated="gvNote_RowCreated">
                    <Columns>
                        <asp:BoundField DataField="RowNumber" HeaderText="SL" />
                        
                        <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <asp:TextBox ID="txtDesc" TextMode="MultiLine" runat="server"></asp:TextBox>

                            </ItemTemplate>

                            <footerstyle />
                        <footertemplate>
                            <asp:LinkButton ID="lbAdd" runat="server" Style="padding: 0;" OnClick="lbAdd_Click" Text="+"></asp:LinkButton>
                        </footertemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" Text="-"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <%-- </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="gvNote" EventName="RowCreated" />
                    </Triggers>
                </asp:UpdatePanel>--%>
            </div>
        </div>

        <br />
        <div class="border-top">
            <div class="card-body" style="text-align: right;">
                <asp:Button class="btn btn-primary" ID="btnSubmitTimeSheet" runat="server" OnClick="btnSubmitTimeSheet_Click" Text="Submit" />
            </div>
        </div>

    </div>

</asp:Content>
