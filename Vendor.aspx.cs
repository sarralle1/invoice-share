﻿using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SrralleIndiaNote
{
    public partial class Vendor : System.Web.UI.Page
    {
        InvoiceShareDBEntities db = new InvoiceShareDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["user"] == null)
                {
                    Response.Redirect("/Login.aspx");
                }
                if (!Page.IsPostBack)
                {
                    FillDropDownListVendor();
                    getGvVendor();
                }
            }
        }


        private void FillDropDownListVendor()
        {
            var cat = db.Suppliers.Select(m => new { id = m.SupplierId, name = m.SupplierName }).ToList();


            ddlFilterVendor.DataSource = cat;
            ddlFilterVendor.DataTextField = "name";
            ddlFilterVendor.DataValueField = "id";
            ddlFilterVendor.DataBind();
            ddlFilterVendor.Items.Insert(0, "Select");



        }

        public void getGvVendor()
        {
            gvVendor.DataSource = null;
            gvVendor.DataBind();
            gvVendor.DataSource = null;
            gvVendor.DataBind();

            if(ddlFilterVendor.SelectedIndex>0)
            {
                string vendor = ddlFilterVendor.SelectedItem.Value;

                var k = db.Suppliers.Where(l=>l.SupplierId==vendor).Select(l => new
                {
                    id = l.SupplierId,
                    name = l.SupplierName
                }).OrderByDescending(l => l.id).ToList();

                gvVendor.DataSource = k;
                gvVendor.DataBind();
            }
            else
            {
                var k = db.Suppliers.Select(l => new
                {
                    id = l.SupplierId,
                    name = l.SupplierName
                }).OrderByDescending(l => l.id).ToList();

                gvVendor.DataSource = k;
                gvVendor.DataBind();
            }
        }


        protected void gvVendor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvVendor.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";

            }
        }

        protected void gvVendor_RowEditing(object sender, GridViewEditEventArgs e)
        {

            gvVendor.EditIndex = e.NewEditIndex;

            this.getGvVendor();

            GridViewRow row = gvVendor.Rows[e.NewEditIndex];
            //(row.Cells[2].Controls[0] as TextBox).Enabled = false;
            (row.Cells[2].Controls[0] as TextBox).Width = new Unit(500);


        }

        protected void gvVendor_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvVendor.EditIndex = -1;
            this.getGvVendor();
        }

        protected void gvVendor_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = gvVendor.Rows[e.RowIndex];
            string id = gvVendor.DataKeys[e.RowIndex].Values[0].ToString();
            Supplier p = db.Suppliers.SingleOrDefault(l => l.SupplierId == id);

            string supplier = (row.Cells[2].Controls[0] as TextBox).Text;


            p.SupplierName = supplier;


            db.SaveChanges();

            gvVendor.EditIndex = -1;
            this.getGvVendor();
        }

        protected void gvVendor_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string id = gvVendor.DataKeys[e.RowIndex].Values[0].ToString();
            db.Suppliers.Where(s => s.SupplierId == id).Delete();
            this.getGvVendor();
        }

        protected void gvVendor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvVendor.PageIndex = e.NewPageIndex;
            getGvVendor();
        }

        protected void btnVendorSubmit_Click(object sender, EventArgs e)
        {
            pnl_err.Visible = false;
            pnl_warning.Visible = false;
            //Save Data
            int flag = 0;
            try
            {
                Supplier t = new Supplier();
                if (txtVendorName.Text != "")
                {
                    t.SupplierId = txtVendorName.Text;
                    t.SupplierName = txtVendorName.Text;
                    db.Suppliers.Add(t);
                }
                else
                    flag = 1;

                if (flag == 1)
                {
                    pnl_err.Visible = true;
                    LabErr.Text = "Blank!!!";
                }
                else
                {
                    //sendMail("","","","","","","");
                    db.SaveChanges();
                    pnl_warning.Visible = true;
                    labWarning.Text = "Success Submitted.";
                    getGvVendor();
                    //Response.Redirect("/WebForm1.aspx");
                }

            }
            catch (Exception ex)
            {

                if (ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    pnl_err.Visible = true;
                    LabErr.Text = ex.InnerException.InnerException.Message;
                }
            }
        }

        protected void ddlFilterVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            getGvVendor();
        }
    }
}