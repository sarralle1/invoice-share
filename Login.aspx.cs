﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SrralleIndiaNote
{
    public partial class Login : System.Web.UI.Page
    {
        InvoiceShareDBEntities db = new InvoiceShareDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {

            }
        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            var log = db.PortalUsers.SingleOrDefault(m => m.Id == txtUserName.Text && m.Pass == txtPassword.Text);
            if(log==null)
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('!!!Invalid user or password')</SCRIPT>");
            else
            {
                if (log.Id == "purchase")
                {
                    Session["user"] = "p";
                    Response.Redirect("/InvoicesShared.aspx");
                }
                    
                else
                {
                    Session["user"] = "a";
                    Response.Redirect("/InvoicesShared.aspx");
                }
                    
            }
                
        }
    }
}