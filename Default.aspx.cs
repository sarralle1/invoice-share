﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SrralleIndiaNote
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Response.Redirect("/Login.aspx");
                //SetInitialRow();
            }
        }

        public void getGrid()
        {

        }


        private void SetInitialRow()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Hours", typeof(string)));
            dt.Columns.Add(new DataColumn("Description", typeof(string)));


            dr = dt.NewRow();
            dr["RowNumber"] = 1;
      
            dr["Description"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState for future reference   
            ViewState["CurrentTable"] = dt;

            //Bind the Gridview   
            gvNote.DataSource = dt;
            gvNote.DataBind();

            ////After binding the gridview, we can then extract and fill the DropDownList with Data   
            //DropDownList ddl1 = (DropDownList)GridView1.Rows[0].Cells[2].FindControl("DropDownList1");

            //FillDropDownList(ddl1);



        }

        protected void gvNote_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                LinkButton lb = (LinkButton)e.Row.FindControl("LinkButton1");
                if (lb != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }
        }
       

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }
        public void AddNewRowToGrid()
        {

            try
            {
                if (pnl_err.Visible != true)
                {
                    if (ViewState["CurrentTable"] != null)
                    {

                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                        DataRow drCurrentRow = null;

                        if (dtCurrentTable.Rows.Count > 0)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                            //add new row to DataTable   
                            dtCurrentTable.Rows.Add(drCurrentRow);
                            //Store the current data to ViewState for future reference   

                            ViewState["CurrentTable"] = dtCurrentTable;


                            for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                            {

                             
                                TextBox box5 = (TextBox)gvNote.Rows[i].Cells[1].FindControl("txtDesc");


                         
                                dtCurrentTable.Rows[i]["Description"] = box5.Text;





                                ////extract the DropDownList Selected Items   

                                //DropDownList ddl1 = (DropDownList)GridView1.Rows[i].Cells[2].FindControl("DropDownList1");

                                //// Update the DataRow with the DDL Selected Items   

                                //dtCurrentTable.Rows[i]["Project"] = ddl1.SelectedItem.Text;

                                //job = ddl1.SelectedItem.Text;
                                //NonEditedFieldLeaveDay(job, box2, box3, box4);


                                //DropDownList ddl2 = (DropDownList)GridView1.Rows[i].Cells[3].FindControl("DropDownList2");
                                //dtCurrentTable.Rows[i]["Activity"] = ddl2.SelectedItem.Text;



                            }

                            //Rebind the Grid with the current data to reflect changes   
                            gvNote.DataSource = dtCurrentTable;
                            gvNote.DataBind();
                        }
                    }
                    else
                    {
                        Response.Write("ViewState is null");

                    }
                    //Set Previous Data on Postbacks   
                    SetPreviousData();
                }

            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }

        private void SetPreviousData()
        {

            try
            {
                int rowIndex = 0;
                if (ViewState["CurrentTable"] != null)
                {

                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                    if (dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                         
                            TextBox box5 = (TextBox)gvNote.Rows[i].Cells[7].FindControl("txtDesc");


                            //DropDownList ddl1 = (DropDownList)GridView1.Rows[rowIndex].Cells[2].FindControl("DropDownList1");
                            //DropDownList ddl2 = (DropDownList)GridView1.Rows[rowIndex].Cells[3].FindControl("DropDownList2");

                            ////Fill the DropDownList with Data   
                            //FillDropDownList(ddl1);

                            //int iu = ddl1.Items.Count;
                            //FillDropDownList2(ddl2);

                            if (i < dt.Rows.Count - 1)
                            {

                                //Assign the value from DataTable to the TextBox   
                              
                                box5.Text = dt.Rows[i]["Description"].ToString();


                                //Set the Previous Selected Items on Each DropDownList  on Postbacks   
                                //ddl1.ClearSelection();
                                //ddl1.Items.FindByText(dt.Rows[i]["Project"].ToString()).Selected = true;
                                //job = ddl1.SelectedValue;
                                //NonEditedFieldLeaveDay(job, box2, box3, box4);
                                //FillDropDownList2(ddl2);


                                //ddl2.ClearSelection();
                                //int iue = ddl2.Items.Count;
                                //ddl2.Items.FindByText(dt.Rows[i]["Activity"].ToString()).Selected = true;

                                // Validation on Calculate MinHour,MaxHour, TimesheetEntry On Leave 
                                pnl_warning.Visible = true;
                                //btnSubmitTimeSheet.Visible = true;
                            }

                            rowIndex++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('!!! " + ex.Message + "')</SCRIPT>");
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (gvRow.RowIndex < dt.Rows.Count - 1)
                    {
                        //Remove the Selected Row data and reset row number  
                        dt.Rows.Remove(dt.Rows[rowID]);
                        ResetRowID(dt);
                    }
                }

                //Store the current data in ViewState for future reference  
                ViewState["CurrentTable"] = dt;

                //Re bind the GridView for the updated data  
                gvNote.DataSource = dt;
                gvNote.DataBind();
            }

            //Set Previous Data on Postbacks  
            SetPreviousData();
        }
        private void ResetRowID(DataTable dt)
        {
            int rowNumber = 1;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    row[0] = rowNumber;
                    rowNumber++;
                }
            }
        }
        protected void btnSubmitTimeSheet_Click(object sender, EventArgs e)
        {

        }
    }
}