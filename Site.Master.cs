﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SrralleIndiaNote
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["user"].ToString() == "p")
                    labUser.Text = "Purchase";
                else if (Session["user"].ToString() == "a")
                    labUser.Text = "Accounts";
            }
        }
    }
}