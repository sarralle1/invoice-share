﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntityFramework.Extensions;

namespace SrralleIndiaNote
{
    public partial class InvoicesShared : System.Web.UI.Page
    {
        InvoiceShareDBEntities db = new InvoiceShareDBEntities();
        string dept = "";
        int received = -1;
        int status = -1;
        string vendor = "Select";
        string invoice = "Select";
        string po = "Select";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];
                if (Session["acReceived"] != null)
                    received = Int32.Parse(Session["acReceived"].ToString());
                if (Session["acStatus"] != null)
                    status = Int32.Parse(Session["acStatus"].ToString());
                if (Session["acVendor"] != null)
                    vendor = Session["acVendor"].ToString();
                if (Session["acInvoice"] != null)
                    invoice = Session["acInvoice"].ToString();
                if (Session["acPo"] != null)
                    po = Session["acPo"].ToString();
            }


            try
            {
                if (Session["user"] == null)
                {
                    Response.Redirect("/Login.aspx");
                }
                if (!Page.IsPostBack)
                {
                    FillDropDownListInvoice();
                    FillDropDownListPO();
                    SetInitialRow();
                    getGvSubmittedInvoices(received, status, vendor, invoice, po);
                }
                dept = Session["user"].ToString();
                if (dept == "a")
                {
                    tabNew.Visible = false;

                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void FillDropDownListInvoice()
        {
            var cat = db.POInvoiceShareds.Select(m => new { id = m.InvoiceNo, name = m.InvoiceNo }).ToList();


            ddlFilterInvoice.DataSource = cat;
            ddlFilterInvoice.DataTextField = "name";
            ddlFilterInvoice.DataValueField = "id";
            ddlFilterInvoice.DataBind();
            ddlFilterInvoice.Items.Insert(0, "Select");

            ddlFilterApprovalInvoice.DataSource = cat;
            ddlFilterApprovalInvoice.DataTextField = "name";
            ddlFilterApprovalInvoice.DataValueField = "id";
            ddlFilterApprovalInvoice.DataBind();
            ddlFilterApprovalInvoice.Items.Insert(0, "Select");

        }

        private void FillDropDownListPO()
        {
            var cat = db.POInvoiceShareds.Select(m => new { id = m.PoNumber, name = m.PoNumber }).ToList();


            ddlFilterPO.DataSource = cat;
            ddlFilterPO.DataTextField = "name";
            ddlFilterPO.DataValueField = "id";
            ddlFilterPO.DataBind();
            ddlFilterPO.Items.Insert(0, "Select");

            ddlFilterApprovalPo.DataSource = cat;
            ddlFilterApprovalPo.DataTextField = "name";
            ddlFilterApprovalPo.DataValueField = "id";
            ddlFilterApprovalPo.DataBind();
            ddlFilterApprovalPo.Items.Insert(0, "Select");

        }
        private void FillDropDownListSupplers(DropDownList ddl)
        {
            var cat = db.Suppliers.Select(m => new { id = m.SupplierId, name = m.SupplierName }).ToList();


            ddl.DataSource = cat;
            ddl.DataTextField = "name";
            ddl.DataValueField = "id";
            ddl.DataBind();
            // ddl.Items.Insert(0, "Select");

            ddlFilterVendor.DataSource = cat;
            ddlFilterVendor.DataTextField = "name";
            ddlFilterVendor.DataValueField = "id";
            ddlFilterVendor.DataBind();
            ddlFilterVendor.Items.Insert(0, "Select");


            ddlFilterApprovalVendor.DataSource = cat;
            ddlFilterApprovalVendor.DataTextField = "name";
            ddlFilterApprovalVendor.DataValueField = "id";
            ddlFilterApprovalVendor.DataBind();
            ddlFilterApprovalVendor.Items.Insert(0, "Select");

        }

        private void SetInitialRow()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Supplier", typeof(string)));
            dt.Columns.Add(new DataColumn("Invoice No", typeof(string)));
            dt.Columns.Add(new DataColumn("Invoice Date", typeof(string)));
            dt.Columns.Add(new DataColumn("Invoice Amount", typeof(string)));
            dt.Columns.Add(new DataColumn("Material Received Date", typeof(string)));
            dt.Columns.Add(new DataColumn("PO Number", typeof(string)));
            dt.Columns.Add(new DataColumn("PO Delivery Date", typeof(string)));
            dt.Columns.Add(new DataColumn("Submission To Accounts Date", typeof(string)));
            dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
            dt.Columns.Add(new DataColumn("ApprovedByAccounts", typeof(string)));
            dt.Columns.Add(new DataColumn("CommentsByAccounts", typeof(string)));


            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Invoice No"] = string.Empty;
            dr["Invoice Date"] = string.Empty;
            dr["Invoice Amount"] = string.Empty;
            dr["Material Received Date"] = 1;
            dr["PO Number"] = string.Empty;
            dr["PO Delivery Date"] = string.Empty;
            dr["Submission To Accounts Date"] = string.Empty;
            dr["Remarks"] = string.Empty;
            dr["ApprovedByAccounts"] = string.Empty;
            dr["CommentsByAccounts"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState for future reference   
            ViewState["CurrentTable"] = dt;

            //Bind the Gridview   
            gvInvoice.DataSource = dt;
            gvInvoice.DataBind();

            //After binding the gridview, we can then extract and fill the DropDownList with Data   
            DropDownList ddl1 = (DropDownList)gvInvoice.Rows[0].Cells[2].FindControl("ddlSupplier");

            FillDropDownListSupplers(ddl1);



        }


        public void AddNewRowToGrid()
        {

            try
            {
                if (pnl_err.Visible != true)
                {
                    if (ViewState["CurrentTable"] != null)
                    {

                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                        DataRow drCurrentRow = null;

                        if (dtCurrentTable.Rows.Count > 0)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                            //add new row to DataTable   
                            dtCurrentTable.Rows.Add(drCurrentRow);
                            //Store the current data to ViewState for future reference   

                            ViewState["CurrentTable"] = dtCurrentTable;


                            for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                            {

                                //extract the TextBox values   


                                TextBox box2 = (TextBox)gvInvoice.Rows[i].Cells[3].FindControl("txtInvoiceNo");
                                TextBox box3 = (TextBox)gvInvoice.Rows[i].Cells[4].FindControl("txtInvoiceDate");
                                TextBox box4 = (TextBox)gvInvoice.Rows[i].Cells[5].FindControl("txtInvoiceAmount");
                                TextBox box5 = (TextBox)gvInvoice.Rows[i].Cells[6].FindControl("txtMaterialReceivedDate");
                                TextBox box6 = (TextBox)gvInvoice.Rows[i].Cells[7].FindControl("txtPoNumber");
                                TextBox box7 = (TextBox)gvInvoice.Rows[i].Cells[8].FindControl("txtPoDeliveryDate");
                                TextBox box8 = (TextBox)gvInvoice.Rows[i].Cells[9].FindControl("txtSubmissionToAccountsDate");
                                TextBox box9 = (TextBox)gvInvoice.Rows[i].Cells[10].FindControl("txtRemarks");




                                dtCurrentTable.Rows[i]["Invoice No"] = box2.Text;
                                dtCurrentTable.Rows[i]["Invoice Date"] = box3.Text;
                                dtCurrentTable.Rows[i]["Invoice Amount"] = box4.Text;
                                dtCurrentTable.Rows[i]["Material Received Date"] = box5.Text;
                                dtCurrentTable.Rows[i]["PO Number"] = box6.Text;
                                dtCurrentTable.Rows[i]["PO Delivery Date"] = box7.Text;
                                dtCurrentTable.Rows[i]["Submission To Accounts Date"] = box8.Text;
                                dtCurrentTable.Rows[i]["Remarks"] = box9.Text;

                                //extract the DropDownList Selected Items   

                                DropDownList ddl1 = (DropDownList)gvInvoice.Rows[i].Cells[2].FindControl("ddlSupplier");

                                // Update the DataRow with the DDL Selected Items   

                                dtCurrentTable.Rows[i]["Supplier"] = ddl1.SelectedItem.Text;


                            }

                            //Rebind the Grid with the current data to reflect changes   
                            gvInvoice.DataSource = dtCurrentTable;
                            gvInvoice.DataBind();
                        }
                    }
                    else
                    {
                        Response.Write("ViewState is null");

                    }
                    //Set Previous Data on Postbacks   
                    SetPreviousData();
                }

            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }

        private void SetPreviousData()
        {

            try
            {
                int rowIndex = 0;
                if (ViewState["CurrentTable"] != null)
                {

                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                    if (dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            TextBox box2 = (TextBox)gvInvoice.Rows[i].Cells[3].FindControl("txtInvoiceNo");
                            TextBox box3 = (TextBox)gvInvoice.Rows[i].Cells[4].FindControl("txtInvoiceDate");
                            TextBox box4 = (TextBox)gvInvoice.Rows[i].Cells[5].FindControl("txtInvoiceAmount");
                            TextBox box5 = (TextBox)gvInvoice.Rows[i].Cells[6].FindControl("txtMaterialReceivedDate");
                            TextBox box6 = (TextBox)gvInvoice.Rows[i].Cells[7].FindControl("txtPoNumber");
                            TextBox box7 = (TextBox)gvInvoice.Rows[i].Cells[8].FindControl("txtPoDeliveryDate");
                            TextBox box8 = (TextBox)gvInvoice.Rows[i].Cells[9].FindControl("txtSubmissionToAccountsDate");
                            TextBox box9 = (TextBox)gvInvoice.Rows[i].Cells[10].FindControl("txtRemarks");


                            DropDownList ddl1 = (DropDownList)gvInvoice.Rows[rowIndex].Cells[2].FindControl("ddlSupplier");
                            // DropDownList ddl2 = (DropDownList)gvInvoice.Rows[rowIndex].Cells[3].FindControl("ddlType");

                            //Fill the DropDownList with Data   
                            FillDropDownListSupplers(ddl1);

                            int iu = ddl1.Items.Count;
                            //FillDropDownList2(ddl2);

                            if (i < dt.Rows.Count - 1)
                            {

                                //Assign the value from DataTable to the TextBox   

                                box2.Text = dt.Rows[i]["Invoice No"].ToString();
                                box3.Text = dt.Rows[i]["Invoice Date"].ToString();
                                box4.Text = dt.Rows[i]["Invoice Amount"].ToString();
                                box5.Text = dt.Rows[i]["Material Received Date"].ToString();
                                box6.Text = dt.Rows[i]["PO Number"].ToString();
                                box7.Text = dt.Rows[i]["PO Delivery Date"].ToString();
                                box8.Text = dt.Rows[i]["Submission To Accounts Date"].ToString();
                                box9.Text = dt.Rows[i]["Remarks"].ToString();


                                //Set the Previous Selected Items on Each DropDownList  on Postbacks   
                                ddl1.ClearSelection();
                                ddl1.Items.FindByText(dt.Rows[i]["Supplier"].ToString()).Selected = true;

                            }

                            rowIndex++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('!!! " + ex.Message + "')</SCRIPT>");
            }
        }


        protected void lbDel_Click(object sender, EventArgs e)
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (gvRow.RowIndex < dt.Rows.Count - 1)
                    {
                        //Remove the Selected Row data and reset row number  
                        dt.Rows.Remove(dt.Rows[rowID]);
                        ResetRowID(dt);
                    }
                }

                //Store the current data in ViewState for future reference  
                ViewState["CurrentTable"] = dt;

                //Re bind the GridView for the updated data  
                gvInvoice.DataSource = dt;
                gvInvoice.DataBind();
            }

            //Set Previous Data on Postbacks  
            SetPreviousData();
        }
        private void ResetRowID(DataTable dt)
        {
            int rowNumber = 1;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    row[0] = rowNumber;
                    rowNumber++;
                }
            }
        }
        protected void gvInvoice_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                ImageButton lb = (ImageButton)e.Row.FindControl("lbDel");
                if (lb != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }
        }

        protected void lbAddNewRow_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //pnl_err.Visible = false;
            //pnl_warning.Visible = false;
            //ViewState["CurrentTable"] = null;
            getAllbeforeSubmit();
            //Save Data
            try
            {
                int rowIndex = 0;
                int flag = 0;
                if (ViewState["CurrentTable"] != null)
                {
                    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                    if (dtCurrentTable.Rows.Count > 0)
                    {

                        for (int i = 1; i < dtCurrentTable.Rows.Count; i++)
                        {
                            POInvoiceShared t = new POInvoiceShared();
                            //extract the TextBox and drop values  


                            DropDownList ddl1 = (DropDownList)gvInvoice.Rows[rowIndex].Cells[2].FindControl("ddlSupplier");
                            TextBox box2 = (TextBox)gvInvoice.Rows[rowIndex].Cells[3].FindControl("txtInvoiceNo");
                            TextBox box3 = (TextBox)gvInvoice.Rows[rowIndex].Cells[4].FindControl("txtInvoiceDate");
                            TextBox box4 = (TextBox)gvInvoice.Rows[rowIndex].Cells[5].FindControl("txtInvoiceAmount");
                            TextBox box5 = (TextBox)gvInvoice.Rows[rowIndex].Cells[6].FindControl("txtMaterialReceivedDate");
                            TextBox box6 = (TextBox)gvInvoice.Rows[rowIndex].Cells[7].FindControl("txtPoNumber");
                            TextBox box7 = (TextBox)gvInvoice.Rows[rowIndex].Cells[8].FindControl("txtPoDeliveryDate");
                            TextBox box8 = (TextBox)gvInvoice.Rows[rowIndex].Cells[9].FindControl("txtSubmissionToAccountsDate");
                            TextBox box9 = (TextBox)gvInvoice.Rows[rowIndex].Cells[10].FindControl("txtRemarks");



                            if (!BlankSupplier())
                            {
                                if (ddl1.SelectedIndex != 0 && box2.Text != "" && box3.Text != "" && box4.Text != "" && box5.Text != "" && box6.Text != "" && box7.Text != ""
                                    && box8.Text != "")
                                {
                                    t.SupplierName = ddl1.SelectedValue;
                                    t.InvoiceNo = box2.Text;
                                    t.InvoiceDate = Convert.ToDateTime(box3.Text);
                                    t.InvoiceAmount = Convert.ToDecimal(box4.Text);
                                    t.MaterialReceivedDate = Convert.ToDateTime(box5.Text);
                                    t.PoNumber = box6.Text;
                                    t.PoDeliveryDate = Convert.ToDateTime(box7.Text);
                                    t.SubmissionToAccountsDate = Convert.ToDateTime(box8.Text);
                                    t.Remarks = box9.Text;

                                    db.POInvoiceShareds.Add(t);

                                    rowIndex++;
                                }
                                else
                                    flag = 1;
                            }
                            else
                                flag = 1;


                        }

                    }
                    db.SaveChanges();
                }
                if (flag == 1)
                {
                    pnl_err.Visible = true;
                    LabErr.Text = "Blank!!!";
                }
                else
                {
                    //sendMail("","","","","","","");
                    pnl_warning.Visible = true;
                    labWarning.Text = "Success Submitted.";
                    pnl_err.Visible = false;
                    this.SetInitialRow();
                    getGvSubmittedInvoices(received, status, vendor, invoice, po);
                    //Response.Redirect("/WebForm1.aspx");
                }

            }
            catch (Exception ex)
            {

                if (ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    pnl_warning.Visible = false;
                    pnl_err.Visible = true;
                    LabErr.Text = ex.InnerException.InnerException.Message;
                }
            }
        }
        private bool BlankSupplier()
        {
            int ct = 0;
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count - 1; i++)
                {

                    string supplier = (string)dt.Rows[i]["Supplier"];


                    if (supplier == "Select" || supplier == "")
                    {
                        ct += 1;
                    }



                }
            }

            if (ct > 0)
                return true;
            else
                return false;

        }


        public void getAllbeforeSubmit()
        {
            try
            {
                if (ViewState["CurrentTable"] != null)
                {

                    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                    DataRow drCurrentRow = null;

                    if (dtCurrentTable.Rows.Count > 0)
                    {
                        if (pnl_err.Visible != true)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                            //add new row to DataTable   
                            dtCurrentTable.Rows.Add(drCurrentRow);
                        }
                        //Store the current data to ViewState for future reference   

                        ViewState["CurrentTable"] = dtCurrentTable;


                        for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                        {

                            //extract the TextBox values   
                            TextBox box2 = (TextBox)gvInvoice.Rows[i].Cells[3].FindControl("txtInvoiceNo");
                            TextBox box3 = (TextBox)gvInvoice.Rows[i].Cells[4].FindControl("txtInvoiceDate");
                            TextBox box4 = (TextBox)gvInvoice.Rows[i].Cells[5].FindControl("txtInvoiceAmount");
                            TextBox box5 = (TextBox)gvInvoice.Rows[i].Cells[6].FindControl("txtMaterialReceivedDate");
                            TextBox box6 = (TextBox)gvInvoice.Rows[i].Cells[7].FindControl("txtPoNumber");
                            TextBox box7 = (TextBox)gvInvoice.Rows[i].Cells[8].FindControl("txtPoDeliveryDate");
                            TextBox box8 = (TextBox)gvInvoice.Rows[i].Cells[9].FindControl("txtSubmissionToAccountsDate");
                            TextBox box9 = (TextBox)gvInvoice.Rows[i].Cells[10].FindControl("txtRemarks");


                            dtCurrentTable.Rows[i]["Invoice No"] = box2.Text;
                            dtCurrentTable.Rows[i]["Invoice Date"] = box3.Text;
                            dtCurrentTable.Rows[i]["Invoice Amount"] = box4.Text;
                            dtCurrentTable.Rows[i]["Material Received Date"] = box5.Text;
                            dtCurrentTable.Rows[i]["PO Number"] = box6.Text;
                            dtCurrentTable.Rows[i]["PO Delivery Date"] = box7.Text;
                            dtCurrentTable.Rows[i]["Submission To Accounts Date"] = box8.Text;
                            dtCurrentTable.Rows[i]["Remarks"] = box9.Text;

                            //extract the DropDownList Selected Items   

                            DropDownList ddl1 = (DropDownList)gvInvoice.Rows[i].Cells[2].FindControl("ddlSupplier");

                            // Update the DataRow with the DDL Selected Items   

                            dtCurrentTable.Rows[i]["Supplier"] = ddl1.SelectedItem.Text;


                        }

                        //Rebind the Grid with the current data to reflect changes   
                        //gvInvoice.DataSource = dtCurrentTable;
                        //gvInvoice.DataBind();
                    }
                }
                else
                {
                    Response.Write("ViewState is null");

                }
            }
            catch (Exception ex)
            {

                pnl_err.Visible = true;
                LabErr.Text = ex.InnerException.InnerException.Message;
            }
        }


        public void getGvSubmittedInvoices(int received, int status, string vendor, string invoice, string po)
        {
            gvSubmittedInvoices.DataSource = null;
            gvSubmittedInvoices.DataBind();
            gvApproval.DataSource = null;
            gvApproval.DataBind();
            if (TabName.Value == "test")
            {
                if (received >= 0)   //Gv Approval Data fill into grid
                {
                    if (received == 2)
                    {
                        var k = db.POInvoiceShareds.Where(m => m.AccountsAcknowledge == true).Select(l => new
                        {
                            id = l.Id,
                            supplier = l.SupplierName,
                            invoiceNo = l.InvoiceNo,
                            invoiceDate = l.InvoiceDate,
                            invoiceAmount = l.InvoiceAmount,
                            materialReceivedDate = l.MaterialReceivedDate,
                            poNumber = l.PoNumber,
                            poDeliveryDate = l.PoDeliveryDate,
                            delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                            submissionToAccountsDate = l.SubmissionToAccountsDate,
                            remarks = l.Remarks,
                            accountsAcknowledge = l.AccountsAcknowledge
                        }).OrderByDescending(l => l.id).ToList();

                        gvApproval.DataSource = k;
                        gvApproval.DataBind();
                    }
                    else if (received == 3)
                    {
                        var k = db.POInvoiceShareds.Where(m => m.AccountsAcknowledge == false || m.AccountsAcknowledge == null).Select(l => new
                        {
                            id = l.Id,
                            supplier = l.SupplierName,
                            invoiceNo = l.InvoiceNo,
                            invoiceDate = l.InvoiceDate,
                            invoiceAmount = l.InvoiceAmount,
                            materialReceivedDate = l.MaterialReceivedDate,
                            poNumber = l.PoNumber,
                            poDeliveryDate = l.PoDeliveryDate,
                            delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                            submissionToAccountsDate = l.SubmissionToAccountsDate,
                            remarks = l.Remarks,
                            accountsAcknowledge = l.AccountsAcknowledge
                        }).OrderByDescending(l => l.id).ToList();

                        gvApproval.DataSource = k;
                        gvApproval.DataBind();
                    }
                    else if (received == 1)
                    {
                        var k = db.POInvoiceShareds.Select(l => new
                        {
                            id = l.Id,
                            supplier = l.SupplierName,
                            invoiceNo = l.InvoiceNo,
                            invoiceDate = l.InvoiceDate,
                            invoiceAmount = l.InvoiceAmount,
                            materialReceivedDate = l.MaterialReceivedDate,
                            poNumber = l.PoNumber,
                            poDeliveryDate = l.PoDeliveryDate,
                            delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                            submissionToAccountsDate = l.SubmissionToAccountsDate,
                            remarks = l.Remarks,
                            accountsAcknowledge = l.AccountsAcknowledge
                        }).OrderByDescending(l => l.id).ToList();

                        gvApproval.DataSource = k;
                        gvApproval.DataBind();
                    }


                }


                if (invoice != "Select")
                {
                    var s = db.POInvoiceShareds.Where(m => m.InvoiceNo == invoice).Select(l => new
                    {
                        id = l.Id,
                        supplier = l.SupplierName,
                        invoiceNo = l.InvoiceNo,
                        invoiceDate = l.InvoiceDate,
                        invoiceAmount = l.InvoiceAmount,
                        materialReceivedDate = l.MaterialReceivedDate,
                        poNumber = l.PoNumber,
                        poDeliveryDate = l.PoDeliveryDate,
                        delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                        submissionToAccountsDate = l.SubmissionToAccountsDate,
                        remarks = l.Remarks,
                        accountsAcknowledge = l.AccountsAcknowledge
                    }).OrderByDescending(l => l.id).ToList();

                    gvApproval.DataSource = s;
                    gvApproval.DataBind();
                }

                else if (po != "Select")
                {
                    var s = db.POInvoiceShareds.Where(m => m.PoNumber == po).Select(l => new
                    {
                        id = l.Id,
                        supplier = l.SupplierName,
                        invoiceNo = l.InvoiceNo,
                        invoiceDate = l.InvoiceDate,
                        invoiceAmount = l.InvoiceAmount,
                        materialReceivedDate = l.MaterialReceivedDate,
                        poNumber = l.PoNumber,
                        poDeliveryDate = l.PoDeliveryDate,
                        delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                        submissionToAccountsDate = l.SubmissionToAccountsDate,
                        remarks = l.Remarks,
                        accountsAcknowledge = l.AccountsAcknowledge
                    }).OrderByDescending(l => l.id).ToList();

                    gvApproval.DataSource = s;
                    gvApproval.DataBind();
                }
                else if (vendor != "Select")
                {
                    var s = db.POInvoiceShareds.Where(m => m.SupplierName == vendor).Select(l => new
                    {
                        id = l.Id,
                        supplier = l.SupplierName,
                        invoiceNo = l.InvoiceNo,
                        invoiceDate = l.InvoiceDate,
                        invoiceAmount = l.InvoiceAmount,
                        materialReceivedDate = l.MaterialReceivedDate,
                        poNumber = l.PoNumber,
                        poDeliveryDate = l.PoDeliveryDate,
                        delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                        submissionToAccountsDate = l.SubmissionToAccountsDate,
                        remarks = l.Remarks,
                        accountsAcknowledge = l.AccountsAcknowledge
                    }).OrderByDescending(l => l.id).ToList();

                    gvApproval.DataSource = s;
                    gvApproval.DataBind();
                }
            }






            if (TabName.Value == "information")
            {
                if (status >= 0)  // Submitted gridview data filled
                {
                    if (status == 2)
                    {
                        var s = db.POInvoiceShareds.Where(m => m.ApprovedByAccounts == true).Select(l => new
                        {
                            id = l.Id,
                            supplier = l.SupplierName,
                            invoiceNo = l.InvoiceNo,
                            invoiceDate = l.InvoiceDate,
                            invoiceAmount = l.InvoiceAmount,
                            materialReceivedDate = l.MaterialReceivedDate,
                            poNumber = l.PoNumber,
                            poDeliveryDate = l.PoDeliveryDate,
                            delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                            submissionToAccountsDate = l.SubmissionToAccountsDate,
                            remarks = l.Remarks,
                            approvedByAccounts = l.ApprovedByAccounts,
                            rejectByAccounts = l.RejectByAccounts,
                            commentsByAccounts = l.CommentsByAccounts
                        }).OrderByDescending(l => l.id).ToList();

                        gvSubmittedInvoices.DataSource = s;
                        gvSubmittedInvoices.DataBind();
                    }
                    else if (status == 3)
                    {
                        var s = db.POInvoiceShareds.Where(m => m.RejectByAccounts == true).Select(l => new
                        {
                            id = l.Id,
                            supplier = l.SupplierName,
                            invoiceNo = l.InvoiceNo,
                            invoiceDate = l.InvoiceDate,
                            invoiceAmount = l.InvoiceAmount,
                            materialReceivedDate = l.MaterialReceivedDate,
                            poNumber = l.PoNumber,
                            poDeliveryDate = l.PoDeliveryDate,
                            delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                            submissionToAccountsDate = l.SubmissionToAccountsDate,
                            remarks = l.Remarks,
                            approvedByAccounts = l.ApprovedByAccounts,
                            rejectByAccounts = l.RejectByAccounts,
                            commentsByAccounts = l.CommentsByAccounts
                        }).OrderByDescending(l => l.id).ToList();

                        gvSubmittedInvoices.DataSource = s;
                        gvSubmittedInvoices.DataBind();
                    }
                    else if (status == 1)
                    {
                        var s = db.POInvoiceShareds.Select(l => new
                        {
                            id = l.Id,
                            supplier = l.SupplierName,
                            invoiceNo = l.InvoiceNo,
                            invoiceDate = l.InvoiceDate,
                            invoiceAmount = l.InvoiceAmount,
                            materialReceivedDate = l.MaterialReceivedDate,
                            poNumber = l.PoNumber,
                            poDeliveryDate = l.PoDeliveryDate,
                            delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                            submissionToAccountsDate = l.SubmissionToAccountsDate,
                            remarks = l.Remarks,
                            approvedByAccounts = l.ApprovedByAccounts,
                            rejectByAccounts = l.RejectByAccounts,
                            commentsByAccounts = l.CommentsByAccounts
                        }).OrderByDescending(l => l.id).ToList();

                        gvSubmittedInvoices.DataSource = s;
                        gvSubmittedInvoices.DataBind();
                    }

                }

                if (invoice != "Select")
                {
                    var s = db.POInvoiceShareds.Where(m => m.InvoiceNo == invoice).Select(l => new
                    {
                        id = l.Id,
                        supplier = l.SupplierName,
                        invoiceNo = l.InvoiceNo,
                        invoiceDate = l.InvoiceDate,
                        invoiceAmount = l.InvoiceAmount,
                        materialReceivedDate = l.MaterialReceivedDate,
                        poNumber = l.PoNumber,
                        poDeliveryDate = l.PoDeliveryDate,
                        delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                        submissionToAccountsDate = l.SubmissionToAccountsDate,
                        remarks = l.Remarks,
                        approvedByAccounts = l.ApprovedByAccounts,
                        rejectByAccounts = l.RejectByAccounts,
                        commentsByAccounts = l.CommentsByAccounts
                    }).OrderByDescending(l => l.id).ToList();

                    gvSubmittedInvoices.DataSource = s;
                    gvSubmittedInvoices.DataBind();
                }
                else if (po != "Select")
                {
                    var s = db.POInvoiceShareds.Where(m => m.PoNumber == po).Select(l => new
                    {
                        id = l.Id,
                        supplier = l.SupplierName,
                        invoiceNo = l.InvoiceNo,
                        invoiceDate = l.InvoiceDate,
                        invoiceAmount = l.InvoiceAmount,
                        materialReceivedDate = l.MaterialReceivedDate,
                        poNumber = l.PoNumber,
                        poDeliveryDate = l.PoDeliveryDate,
                        delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                        submissionToAccountsDate = l.SubmissionToAccountsDate,
                        remarks = l.Remarks,
                        approvedByAccounts = l.ApprovedByAccounts,
                        rejectByAccounts = l.RejectByAccounts,
                        commentsByAccounts = l.CommentsByAccounts
                    }).OrderByDescending(l => l.id).ToList();

                    gvSubmittedInvoices.DataSource = s;
                    gvSubmittedInvoices.DataBind();
                }
                else if (vendor != "Select")
                {
                    var s = db.POInvoiceShareds.Where(m => m.SupplierName == vendor).Select(l => new
                    {
                        id = l.Id,
                        supplier = l.SupplierName,
                        invoiceNo = l.InvoiceNo,
                        invoiceDate = l.InvoiceDate,
                        invoiceAmount = l.InvoiceAmount,
                        materialReceivedDate = l.MaterialReceivedDate,
                        poNumber = l.PoNumber,
                        poDeliveryDate = l.PoDeliveryDate,
                        delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
                        submissionToAccountsDate = l.SubmissionToAccountsDate,
                        remarks = l.Remarks,
                        approvedByAccounts = l.ApprovedByAccounts,
                        rejectByAccounts = l.RejectByAccounts,
                        commentsByAccounts = l.CommentsByAccounts
                    }).OrderByDescending(l => l.id).ToList();

                    gvSubmittedInvoices.DataSource = s;
                    gvSubmittedInvoices.DataBind();
                } 
            }


        }

        protected void gvSubmittedInvoices_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvSubmittedInvoices.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";

            }

        }

        protected void gvSubmittedInvoices_RowEditing(object sender, GridViewEditEventArgs e)
        {
            TabName.Value = "information";
            dept = Session["user"].ToString();
            gvSubmittedInvoices.EditIndex = e.NewEditIndex;

            this.getGvSubmittedInvoices(received, status, vendor, invoice, po);

            GridViewRow row = gvSubmittedInvoices.Rows[e.NewEditIndex];
            CheckBox accountApprove = row.Cells[12].Controls[1] as CheckBox;
            CheckBox accountReject = row.Cells[13].Controls[1] as CheckBox;
            (row.Cells[2].Controls[0] as TextBox).Enabled = false;
            (row.Cells[9].Controls[0] as TextBox).Enabled = false;
            (row.Cells[12].Controls[1] as CheckBox).Enabled = false;
            (row.Cells[13].Controls[1] as CheckBox).Enabled = false;
            (row.Cells[14].Controls[1] as TextBox).Enabled = false;
            if (!accountReject.Checked && !accountApprove.Checked && dept == "p")
            {
                return;
            }
            else if (!accountReject.Checked && dept == "p")
            {
                (row.Cells[12].Controls[1] as TextBox).Enabled = false; // Error statement cell 12 having checkbox !!! do not nothing need to write error statement . this is crazy.
            }
            else if (dept == "a")
            {

                (row.Cells[3].Controls[0] as TextBox).Enabled = false;
                (row.Cells[4].Controls[0] as TextBox).Enabled = false;
                (row.Cells[5].Controls[0] as TextBox).Enabled = false;
                (row.Cells[6].Controls[0] as TextBox).Enabled = false;
                (row.Cells[7].Controls[0] as TextBox).Enabled = false;
                (row.Cells[8].Controls[0] as TextBox).Enabled = false;
                (row.Cells[10].Controls[0] as TextBox).Enabled = false;
                (row.Cells[11].Controls[0] as TextBox).Enabled = false;

                (row.Cells[12].Controls[1] as CheckBox).Enabled = true;
                (row.Cells[13].Controls[1] as CheckBox).Enabled = true;
                (row.Cells[14].Controls[1] as TextBox).Enabled = true;

            }

        }

        protected void gvSubmittedInvoices_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            TabName.Value = "information";
            gvSubmittedInvoices.EditIndex = -1;
            this.getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }

        protected void gvSubmittedInvoices_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            TabName.Value = "information";
            dept = Session["user"].ToString();
            GridViewRow row = gvSubmittedInvoices.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvSubmittedInvoices.DataKeys[e.RowIndex].Values[0]);
            POInvoiceShared p = db.POInvoiceShareds.SingleOrDefault(l => l.Id == id);
            if (dept == "a")
            {
                bool approveByAccount = (row.Cells[12].Controls[1] as CheckBox).Checked;
                bool rejectByAccount = (row.Cells[13].Controls[1] as CheckBox).Checked;
                string accountComments = (row.Cells[14].Controls[1] as TextBox).Text;

                p.ApprovedByAccounts = approveByAccount;
                p.RejectByAccounts = rejectByAccount;
                p.CommentsByAccounts = accountComments;
            }
            else
            {
                string supplier = (row.Cells[2].Controls[0] as TextBox).Text;
                //string supplier = (row.Cells[2].Controls[0] as DropDownList).SelectedValue;
                string invoiceNo = (row.Cells[3].Controls[0] as TextBox).Text;
                string invoiceDate = (row.Cells[4].Controls[0] as TextBox).Text;
                string invoiceAmount = (row.Cells[5].Controls[0] as TextBox).Text;
                string materialReceivedDate = (row.Cells[6].Controls[0] as TextBox).Text;
                string poNumber = (row.Cells[7].Controls[0] as TextBox).Text;
                string poDeliveryDate = (row.Cells[8].Controls[0] as TextBox).Text;
                string delay = (row.Cells[9].Controls[0] as TextBox).Text;
                string submissionToAccountsDate = (row.Cells[10].Controls[0] as TextBox).Text;
                string remarks = (row.Cells[11].Controls[0] as TextBox).Text;

                p.SupplierName = supplier;
                p.InvoiceNo = invoiceNo;
                p.InvoiceDate = Convert.ToDateTime(invoiceDate);
                p.InvoiceAmount = Convert.ToDecimal(invoiceAmount);
                p.MaterialReceivedDate = Convert.ToDateTime(materialReceivedDate);//Convert.ToDateTime();
                p.PoNumber = poNumber;
                p.PoDeliveryDate = Convert.ToDateTime(poDeliveryDate);
                p.SubmissionToAccountsDate = Convert.ToDateTime(submissionToAccountsDate);
                p.Remarks = remarks;

            }

            db.SaveChanges();

            gvSubmittedInvoices.EditIndex = -1;
            this.getGvSubmittedInvoices(received, status, vendor, invoice, po);
            //Response.Redirect("/InvoicesShared.aspx");
        }

        protected void gvSubmittedInvoices_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            TabName.Value = "information";
            dept = Session["user"].ToString();
            if (dept == "")
            {
                Response.Redirect("/InvoicesShared.aspx");
            }
            if (dept == "p")
            {
                int id = Convert.ToInt32(gvSubmittedInvoices.DataKeys[e.RowIndex].Values[0]);
                db.POInvoiceShareds.Where(s => s.Id == id).Delete();
                this.getGvSubmittedInvoices(received, status, vendor, invoice, po);
            }
                
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('!!! Hello world!!')</SCRIPT>");
        }

        protected void gvSubmittedInvoices_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            TabName.Value = "information";
            gvSubmittedInvoices.PageIndex = e.NewPageIndex;
            getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }

        protected void gvApproval_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvApproval.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }

        }

        protected void gvApproval_RowEditing(object sender, GridViewEditEventArgs e)
        {
            TabName.Value = "test";
            dept = Session["user"].ToString();
            gvApproval.EditIndex = e.NewEditIndex;

            this.getGvSubmittedInvoices(received, status, vendor, invoice, po);
            GridViewRow row = gvApproval.Rows[e.NewEditIndex];
            CheckBox accountReject = row.Cells[12].Controls[0] as CheckBox;
            (row.Cells[2].Controls[0] as TextBox).Enabled = false;
            (row.Cells[9].Controls[0] as TextBox).Enabled = false;
            (row.Cells[3].Controls[0] as TextBox).Enabled = false;
            (row.Cells[4].Controls[0] as TextBox).Enabled = false;
            (row.Cells[5].Controls[0] as TextBox).Enabled = false;
            (row.Cells[6].Controls[0] as TextBox).Enabled = false;
            (row.Cells[7].Controls[0] as TextBox).Enabled = false;
            (row.Cells[8].Controls[0] as TextBox).Enabled = false;
            (row.Cells[10].Controls[0] as TextBox).Enabled = false;
            (row.Cells[11].Controls[0] as TextBox).Enabled = false;
            (row.Cells[12].Controls[1] as CheckBox).Enabled = false;

            if (dept == "p")
            {
                (row.Cells[12].Controls[1] as TextBox).Enabled = false; // Error statement cell 12 having checkbox !!! do not nothing need to write error statement . this is crazy.    
                // System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('The Invoice only editable once rejected by Accounts')</SCRIPT>");
                //Response.Redirect("/InvoicesShared.aspx");\
                //gvApproval.EditIndex = -1;
                //this.getGvSubmittedInvoices(received, status,vendor,invoice,po);
            }
            else if (dept == "a")
            {
                (row.Cells[12].Controls[1] as CheckBox).Enabled = true;
            }
        }

        protected void gvApproval_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            TabName.Value = "test";
            gvApproval.EditIndex = -1;
            this.getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }

        protected void gvApproval_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            TabName.Value = "test";
            dept = Session["user"].ToString();
            if (dept == "")
            {
                Response.Redirect("/InvoicesShared.aspx");
            }
            if (dept == "p")
            {
                int id = Convert.ToInt32(gvApproval.DataKeys[e.RowIndex].Values[0]);
                db.POInvoiceShareds.Where(s => s.Id == id).Delete();

                this.getGvSubmittedInvoices(received, status, vendor, invoice, po);
            }
                
        }

        protected void gvApproval_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            TabName.Value = "test";
            dept = Session["user"].ToString();
            GridViewRow row = gvApproval.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvApproval.DataKeys[e.RowIndex].Values[0]);
            POInvoiceShared p = db.POInvoiceShareds.SingleOrDefault(l => l.Id == id);
            if (dept == "a")
            {
                bool receivedByAccount = (row.Cells[12].Controls[1] as CheckBox).Checked;

                p.AccountsAcknowledge = receivedByAccount;
            }


            db.SaveChanges();


            gvApproval.EditIndex = -1;
            this.getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }

        protected void gvApproval_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            TabName.Value = "test";
            gvApproval.PageIndex = e.NewPageIndex;
            getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }

        protected void chkApprove_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRow row = (sender as CheckBox).NamingContainer as GridViewRow;

            (row.Cells[12].Controls[1] as CheckBox).Checked = true;

            (row.Cells[13].Controls[1] as CheckBox).Checked = false;


        }

        protected void chkReject_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRow row = (sender as CheckBox).NamingContainer as GridViewRow;

            (row.Cells[12].Controls[1] as CheckBox).Checked = false;

            (row.Cells[13].Controls[1] as CheckBox).Checked = true;
        }

        protected void ddlAccountsAcknowledge_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "test";
            ddlFilterApprovalInvoice.SelectedIndex = 0;
            ddlFilterApprovalVendor.SelectedIndex = 0;
            ddlFilterApprovalPo.SelectedIndex = 0;
            received = ddlAccountsAcknowledge.SelectedIndex;
            vendor = ddlFilterApprovalVendor.SelectedItem.Value;
            invoice = ddlFilterApprovalInvoice.SelectedItem.Value;
            po = ddlFilterApprovalPo.SelectedItem.Value;
            Session["acReceived"] = received;
            Session["acVendor"] = vendor;
            Session["acInvoice"] = invoice;
            Session["acPo"] = po;
            
            getGvSubmittedInvoices(received, status, vendor, invoice, po);

        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "information";
            ddlFilterInvoice.SelectedIndex = 0;
            ddlFilterVendor.SelectedIndex = 0;
            ddlFilterPO.SelectedIndex = 0;
            status = ddlStatus.SelectedIndex;
            vendor = ddlFilterVendor.SelectedItem.Value;
            invoice = ddlFilterInvoice.SelectedItem.Value;
            po = ddlFilterPO.SelectedItem.Value;
            Session["acStatus"] = status;
            Session["acVendor"] = vendor;
            Session["acInvoice"] = invoice;
            Session["acPo"] = po;
            
            getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }

        protected void ddlFilterVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "information";
            ddlStatus.SelectedIndex = 0;
            ddlFilterInvoice.SelectedIndex = 0;
            ddlFilterPO.SelectedIndex = 0;
            status = ddlStatus.SelectedIndex;
            vendor = ddlFilterVendor.SelectedItem.Value;
            invoice = ddlFilterInvoice.SelectedItem.Value;
            po = ddlFilterPO.SelectedItem.Value;
            Session["acStatus"] = status;
            Session["acVendor"] = vendor;
            Session["acInvoice"] = invoice;
            Session["acPo"] = po;
           
            getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }

        protected void ddlFilterInvoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "information";
            ddlStatus.SelectedIndex = 0;
            ddlFilterVendor.SelectedIndex = 0;
            ddlFilterPO.SelectedIndex = 0;
            status = ddlStatus.SelectedIndex;
            vendor = ddlFilterVendor.SelectedItem.Value;
            invoice = ddlFilterInvoice.SelectedItem.Value;
            po = ddlFilterPO.SelectedItem.Value;
            Session["acStatus"] = status;
            Session["acVendor"] = vendor;
            Session["acInvoice"] = invoice;
            Session["acPo"] = po;
            
            getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }

        protected void ddlFilterPO_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "information";
            ddlStatus.SelectedIndex = 0;
            ddlFilterInvoice.SelectedIndex = 0;
            ddlFilterVendor.SelectedIndex = 0;
            status = ddlStatus.SelectedIndex;
            vendor = ddlFilterVendor.SelectedItem.Value;
            invoice = ddlFilterInvoice.SelectedItem.Value;
            po = ddlFilterPO.SelectedItem.Value;
            Session["acStatus"] = status;
            Session["acVendor"] = vendor;
            Session["acInvoice"] = invoice;
            Session["acPo"] = po;
            
            getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }

        protected void ddlFilterApprovalVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "test";
            ddlAccountsAcknowledge.SelectedIndex = 0;
            ddlFilterApprovalInvoice.SelectedIndex = 0;
            ddlFilterApprovalPo.SelectedIndex = 0;
            received = ddlAccountsAcknowledge.SelectedIndex;
            vendor = ddlFilterApprovalVendor.SelectedItem.Value;
            invoice = ddlFilterApprovalInvoice.SelectedItem.Value;
            Session["acReceived"] = received;
            Session["acVendor"] = vendor;
            Session["acInvoice"] = invoice;
            Session["acPo"] = po;
            
            getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }

        protected void ddlFilterApprovalInvoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "test";
            ddlAccountsAcknowledge.SelectedIndex = 0;
            ddlFilterApprovalVendor.SelectedIndex = 0;
            ddlFilterApprovalPo.SelectedIndex = 0;
            received = ddlAccountsAcknowledge.SelectedIndex;
            vendor = ddlFilterApprovalVendor.SelectedItem.Value;
            invoice = ddlFilterApprovalInvoice.SelectedItem.Value;
            Session["acReceived"] = received;
            Session["acVendor"] = vendor;
            Session["acInvoice"] = invoice;
            Session["acPo"] = po;
            
            getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }

        protected void ddlFilterApprovalPo_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "test";
            ddlAccountsAcknowledge.SelectedIndex = 0;
            ddlFilterApprovalInvoice.SelectedIndex = 0;
            ddlFilterApprovalVendor.SelectedIndex = 0;
            received = ddlAccountsAcknowledge.SelectedIndex;
            vendor = ddlFilterApprovalVendor.SelectedItem.Value;
            invoice = ddlFilterApprovalInvoice.SelectedItem.Value;
            Session["acReceived"] = received;
            Session["acVendor"] = vendor;
            Session["acInvoice"] = invoice;
            Session["acPo"] = po;
            
            getGvSubmittedInvoices(received, status, vendor, invoice, po);
        }
        // duplicate check----- current table with submited po table checked duplicate validation




        //public void MergeExistWithCurrent()
        //{
        //    var k = db.POInvoiceShareds.Select(l => new
        //    {
        //        id = l.Id,
        //        supplier = l.SupplierName,
        //        invoiceNo = l.InvoiceNo,
        //        invoiceDate = l.InvoiceDate,
        //        invoiceAmount = l.InvoiceAmount,
        //        materialReceivedDate = l.MaterialReceivedDate,
        //        poNumber = l.PoNumber,
        //        poDeliveryDate = l.PoDeliveryDate,
        //        delay = DbFunctions.DiffDays(l.PoDeliveryDate, l.MaterialReceivedDate),
        //        submissionToAccountsDate = l.SubmissionToAccountsDate,
        //        remarks = l.Remarks,
        //        accountsAcknowledge = l.AccountsAcknowledge
        //    }).ToList();



        //    DataTable dt = new DataTable();
        //    POInvoiceShared[] ll = db.POInvoiceShareds.ToArray();
        //    DataSet ds = SubmittedPO(ll);
        //    dt = (DataTable)ViewState["SubmittedPO"];
        //    if (ViewState["CurrentTable"] != null)
        //    {
        //        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
        //        dt.Merge(dtCurrentTable);
        //    }

        //    ViewState["MergedTable"] = dt;
        //}



        //public DataSet SubmittedPO(POInvoiceShared[] ld)
        //{
        //    try
        //    {

        //        DataTable dt = new DataTable();

        //        dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Supplier", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Invoice No", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Invoice Date", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Invoice Amount", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Material Received Date", typeof(string)));
        //        dt.Columns.Add(new DataColumn("PO Number", typeof(string)));
        //        dt.Columns.Add(new DataColumn("PO Delivery Date", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Submission To Accounts Date", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
        //        dt.Columns.Add(new DataColumn("ApprovedByAccounts", typeof(string)));
        //        dt.Columns.Add(new DataColumn("CommentsByAccounts", typeof(string)));

        //        foreach (POInvoiceShared l in ld)
        //        {
        //            DataRow dr = dt.NewRow();

        //            //dr["RowNumber"] = 1;
        //            dr["Invoice No"] = l.InvoiceNo;
        //            dr["Invoice Date"] = l.InvoiceDate;
        //            dr["Invoice Amount"] = l.InvoiceAmount;
        //            dr["Material Received Date"] = l.MaterialReceivedDate;
        //            dr["PO Number"] = l.PoNumber;
        //            dr["PO Delivery Date"] = l.PoDeliveryDate;
        //            dr["Submission To Accounts Date"] = l.SubmissionToAccountsDate;
        //            dr["Remarks"] = l.Remarks;
        //            dr["ApprovedByAccounts"] = l.ApprovedByAccounts;
        //            dr["CommentsByAccounts"] = l.CommentsByAccounts;

        //            dt.Rows.Add(dr);
        //        }

        //        DataSet dd = new DataSet();
        //        dd.Tables.Add(dt);
        //        ViewState["SubmittedPO"] = dt;
        //        return dd;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        //protected void txtPoNumber_TextChanged(object sender, EventArgs e)
        //{
        //    TextBox txt = (TextBox)sender;
        //    GridViewRow row = (GridViewRow)txt.NamingContainer;


        //    TextBox txtPoNumber = (TextBox)gvInvoice.Rows[row.RowIndex].Cells[7].FindControl("txtPoNumber");

        //    MergeExistWithCurrent();


        //}
    }
}